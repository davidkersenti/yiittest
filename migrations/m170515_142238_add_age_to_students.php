<?php

use yii\db\Migration;

class m170515_142238_add_age_to_students extends Migration
{
    public function up()
    {
		$this->addColumn('student', 'age', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('student', 'age');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
